provider "aws" {
  region     = "${var.aws_region}"
}

#
# VPC related resources
#

resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${var.aws_region}${var.aws_availability_zone}"
}

resource "aws_subnet" "private" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "10.0.100.0/24"
  availability_zone = "${var.aws_region}${var.aws_availability_zone}"
}

#
# Security Groups related resources
#

resource "aws_security_group" "webserver" {
  name        = "webserver-sg"
  description = "Opens port 80 for HTTP access."
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "pgsql" {
  name        = "pgsql-sg"
  description = "Opens port 5432 for PostgreSQL access."
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = ["${aws_security_group.webserver.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#
# EC2 related resources
#

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "webserver" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  availability_zone      = "${var.aws_region}${var.aws_availability_zone}"
  vpc_security_group_ids = ["${aws_security_group.webserver.id}"]
  subnet_id              = "${aws_subnet.public.id}"

  tags {
    Name        = "ec2-webserver"
    Description = "EC2 instance that represents a webserver."
  }
}

resource "aws_instance" "database" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  availability_zone      = "${var.aws_region}${var.aws_availability_zone}"
  vpc_security_group_ids = ["${aws_security_group.pgsql.id}"]
  subnet_id              = "${aws_subnet.private.id}"

  tags {
    Name        = "ec2-db-pgsql"
    Description = "EC2 instance that represents a PostgreSQL database."
  }
}
