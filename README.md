# Testing AWS Infrastructure using Kitchen CI

This is a bery small example, using one of the several tools, that allows us to test infrastructure. Please be aware that I'm not trying to show the best practices, I just want to show the possibilities.

## The tools I've used

The tools I've used in this example are:

* As a cloud provider I went for [AWS](https://aws.amazon.com/). The reason why I chose AWS, because I'm more used to it.
* As a testing framework I used [awspec](https://github.com/k1LoW/awspec/). I was undecided betweeen `awspec` and [InSpec](https://www.inspec.io/), but because `awspec` has more testable resources out-of-the box, I thought it would be better.

## The example

The following diagram shows the infrastructure I'm going to be testing.

![Infrastructure diagram](./docs/AWS-Diagram.png)

The `VPC` cidr blocks is `10.0.0.0/16`.

It has 2 subnets: a public and a private. The public subnet has an IP range of `10.0.1.0/24` and the private subnet as an IP range of `10.0.100.0/24`.

There are 2 `EC2` instances, one in the public subnet that represents the webserver, and another one in the private subnet, that represents the database server.

Each `EC2` instance has one security group. The *webserver* security group allows access from the outside world through port `80` for HTTP access and port `22` for SSH access (to upload files, for example). The *database* security group only allows access through port `5432` (port used by PostgreSQL) and from the *webserver* security group.

## Pre-requisites

* An AWS account (with an access key ID and secret key)
* Terraform [https://www.terraform.io/downloads.html](https://www.terraform.io/downloads.html)
* Ruby [https://www.ruby-lang.org/en/downloads/](https://www.ruby-lang.org/en/downloads/)
* Bundler [https://bundler.io/](https://bundler.io/)

## Run this example

Follow the steps bellow to run this example.

### Pre-requisites

#### Install Terraform

Go to Terraform's download [page](https://www.terraform.io/downloads.html) and download the appropriate release.

### Install Ruby

Go to Ruby's download [page](https://www.ruby-lang.org/en/documentation/installation/) and download the release for your OS.

#### Configure AWS

To run this example, you need to have an AWS account and create a programmatic access user.

After creating the user, you need to setup the variables `aws_access_key` and `aws_secret_key` (you can find them in the `variables.tf` file).

### Cloning the repo

```bash
git clone https://github.com/hmnp/example-how-to-test-infrastructure-using-awspec
```

### Install the required gems

The file `Gemfile` defines the required `gems`.

```bash
bundle install
```

### Create the infrastructure

```bash
kitchen converge
```

### Test the infrastructure

```bash
kitchen verify
```

### Destroy the infrastructure

```bash
kitchen destroy
```
